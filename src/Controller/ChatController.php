<?php

namespace App\Controller;

use App\Entity\Chat;
use App\Form\ChatType;
use App\Repository\ChatRepository;
use App\Repository\UserRepository;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/chat")
 */
class ChatController extends AbstractController
{
    /**
     * @Route("/", name="chat_index", methods="GET|POST")
     */
    public function index(Request $request, ChatRepository $chatRepository, UserRepository $userRepository): Response
    {
        $nMessageDefault = 10;

        $addNMessages = $request->request->get("add_n_messages");
        dump("requeste : " . $addNMessages);

        if ($addNMessages === null) {
            $addNMessages = $request->getSession()->get("addNMessages");
            dump("recup session : " . $addNMessages);
        } else {
            $request->getSession()->set("addNMessages", $addNMessages);
            dump("ajouter session : " . $addNMessages);
        }

        $nMessages = $nMessageDefault + $addNMessages;

        $chats = $chatRepository->findBy([], ["at" => "DESC"], $nMessages);
        $usersOnline = $userRepository->findBy(["online" => true], ["username" => "ASC"]);

        return $this->render('chat/index.html.twig', [
            'chats' => $chats,
            'usersOnline' => $usersOnline
        ]);
    }

    /**
     * @Route("/new", name="chat_new", methods="GET|POST")
     * @Security("has_role('ROLE_USER')")
     */
    public function new(Request $request): Response
    {
        $chat = new Chat();
        $chat->setMessage($request->request->get("message"));
        $chat->setUser($this->getUser());
        $chat->setAt(new DateTime("now"));

        $em = $this->getDoctrine()->getManager();

        $em->persist($chat);
        $em->flush();

        return $this->redirectToRoute('chat_index');
    }

    /**
     * @Route("/{id}", name="chat_show", methods="GET")
     */
    public function show(Chat $chat): Response
    {
        return $this->render('chat/show.html.twig', ['chat' => $chat]);
    }

    /**
     * @Route("/{id}/edit", name="chat_edit", methods="GET|POST")
     */
    public function edit(Request $request, Chat $chat): Response
    {
        $form = $this->createForm(ChatType::class, $chat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('chat_edit', ['id' => $chat->getId()]);
        }

        return $this->render('chat/edit.html.twig', [
            'chat' => $chat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="chat_delete", methods="DELETE")
     */
    public function delete(Request $request, Chat $chat): Response
    {
        if ($this->isCsrfTokenValid('delete' . $chat->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($chat);
            $em->flush();
        }

        return $this->redirectToRoute('chat_index');
    }
}
