<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManagerService
{
    private $objectManager;
    private $session;
    private $encoder;

    public function __construct(ObjectManager $objectManager, UserPasswordEncoderInterface $encoder)
    {
        $this->objectManager = $objectManager;
        $this->session = new Session();
        $this->encoder = $encoder;
    }

    protected function getRepository()
    {
        return $this->objectManager->getRepository(User::class);
    }

    protected function uniqueUsername(string $username, int $iduser = 0)
    {
        $testExist = $this->getRepository()->findOneBy(["username" => $username]);

        if (($testExist) && ($testExist->getId() !== $iduser)) {
            $this->session->getFlashBag()
                ->add('danger', 'Username "' . $username . '" existe déjà choisir un autre');
            return false;
        } else {
            return true;
        }
    }

    protected function randomUsername()
    {
        $baseUsername = [
            "User",
            "JohnDoe",
            "JaneDoe"
        ];
        $endUsername = rand(0, 1000);

        $randomUsername = $baseUsername[array_rand($baseUsername)] . $endUsername;

        return $randomUsername;
    }

    public function listeUsernames(int $numberUsername = 3)
    {
        $this->randomUsername();

        $listUniqueUsernames = [];

        do {
            $randomUsername = $this->randomUsername();
            if ($this->uniqueUsername($randomUsername) === true) {
                $listUniqueUsernames [] = $randomUsername;
            }
        } while (count($listUniqueUsernames) != $numberUsername);

        return $listUniqueUsernames;
    }

    public function saveNewUser(User $user)
    {
        if ($this->uniqueUsername($user->getUsername()) === false) {
            return false;
        }

        $plainpassword = $user->getPassword();
        $user->setPassword(
            $this->encoder->encodePassword($user, $plainpassword)
        );

        $user->setOnline(false);
        $user->setRoles(["ROLE_USER"]);

        $this->objectManager->persist($user);
        $this->objectManager->flush();

        return true;
    }

    public function saveUpdateUser(User $user)
    {
        if ($this->uniqueUsername($user->getUsername(), $user->getId()) === true) {

            $plainpassword = $user->getPassword();

            $user->setPassword(
                $this->encoder->encodePassword($user, $plainpassword)
            );

            $this->objectManager->flush();

            $this->session->getFlashBag()
                ->add('success', 'Profil mis à jour.');
        }
    }
}